package com.relevance.student.studentapplication.student;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.relevance.student.studentapplication.cclass.Cclass;

@Entity
public class Student {
	@Id
	String student_id;
	String student_fname;
	String student_lname;
	String student_email;

	@ManyToOne
	@JoinColumn (name="cclass_id")
	private Cclass cclass;
	public Cclass getCclass() {
		return cclass;
	}





	public void setCclass(Cclass cclass) {
		this.cclass = cclass;
	}





	public Student(String student_id, String student_fname, String student_lname, String student_email,String cclassId) {
		super();
		this.student_id = student_id;
		this.student_fname = student_fname;
		this.student_lname = student_lname;
		this.student_email = student_email;
		this.cclass=new Cclass(cclassId,"","");
	}



	
	
	public Student()  {}


	public String getStudent_id() {
		return student_id;
	}


	public void setStudent_id(String student_id) {
		this.student_id = student_id;
	}


	public String getStudent_fname() {
		return student_fname;
	}


	public void setStudent_fname(String student_fname) {
		this.student_fname = student_fname;
	}


	public String getStudent_lname() {
		return student_lname;
	}


	public void setStudent_lname(String student_lname) {
		this.student_lname = student_lname;
	}


	public String getStudent_email() {
		return student_email;
	}


	public void setStudent_email(String student_email) {
		this.student_email = student_email;
	}

}
