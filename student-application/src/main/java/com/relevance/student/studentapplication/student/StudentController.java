package com.relevance.student.studentapplication.student;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.relevance.student.studentapplication.cclass.Cclass;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class StudentController {
	
	@Autowired
	 private StudentService studentservice;
	
	
	//all student in all classes
	@RequestMapping("/students")
	 public List<Student> getAllStudents() {
		return studentservice.getAllStudents();
	}
		
		//particular student list on basis of class
	@RequestMapping("/topics/{topicId}/cclass/{cclass_id}/student")
	 public List<Student> getAllStudent(@PathVariable String topicId ,@PathVariable String cclass_id) {
		return studentservice.getAllStudent(cclass_id);
	}
	
	@RequestMapping("/topics/{topicId}/cclass/{cclass_id}/student/{student_id}")
	public  Optional<Student> getStudent(@PathVariable String student_id ) {
		return  studentservice.getStudent(student_id);
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/topics/{topicId}/cclass/{cclassId}/student")
	public void addStudent(@RequestBody Student student,@PathVariable String topicId ,@PathVariable String cclassId) {
		student.setCclass(new Cclass(cclassId,"",""));
		studentservice.addStudent(student); 
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/topics/{topicId}/cclass/{cclassId}/student/{student_id}")
	public void updateStudent(@RequestBody Student student,@PathVariable String student_id,@PathVariable String topicId ,@PathVariable String cclassId) {
		student.setCclass(new Cclass(cclassId,"",""));
		studentservice.updateStudent(student,student_id);
		
	}
	
	
	@RequestMapping(method=RequestMethod.DELETE,value="/topics/{topicId}/cclass/{cclass_id}/student/{student_id}")
	 public void deleteStudent(@PathVariable String student_id) {
		studentservice.deleteStudent(student_id);
	}
	

}
