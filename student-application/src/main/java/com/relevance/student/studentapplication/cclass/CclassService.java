package com.relevance.student.studentapplication.cclass;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CclassService {
	@Autowired
	private CclassRepostiary courseRepositary;
	 
	 public List<Cclass> getAllCclass(String topicId){
		 List<Cclass> topics= new ArrayList<>();
		 
		 courseRepositary.findByTopicId(topicId).forEach(topics::add);
		 return topics;
	 }
	 public Optional<Cclass> getCourse(String cclass_id) {
		// return topics.stream().filter(t -> t.getId().equals(id)).findFirst().get();
		 
		 return courseRepositary.findById(cclass_id);
	 }
	public void addCourse(Cclass topic) {
		
		courseRepositary.save(topic);
	}
	public void updateCourse( Cclass cclass) {
		courseRepositary.save(cclass);
	}
	public void deleteCourse(String cclass_id) {
		courseRepositary.deleteById(cclass_id);
		
	}

}


//package com.relevance.student.studentapplication.cclass;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Optional;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//@Service
//public class CclassService {
//	@Autowired
//	private CclassRepostiary courseRepositary;
//	 
//	 public List<Cclass> getAllCclass(String topicId){
//		 List<Cclass> topics= new ArrayList<>();
//		 
//		 courseRepositary.findByTopicId(topicId).forEach(topics::add);
//		 return topics;
//	 }
//	 public Optional<Cclass> getCourse(String cclass_id) {
//		// return topics.stream().filter(t -> t.getId().equals(id)).findFirst().get();
//		 
//		 return courseRepositary.findById(cclass_id);
//	 }
//	public void addCourse(Cclass topic) {
//		
//		courseRepositary.save(topic);
//	}
//	public void updateCourse( Cclass cclass) {
//		courseRepositary.save(cclass);
//	}
//	public void deleteCourse(String cclass_id) {
//		courseRepositary.deleteById(cclass_id);
//		
//	}
//
//}
