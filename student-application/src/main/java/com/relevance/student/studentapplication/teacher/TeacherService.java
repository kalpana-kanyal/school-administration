package com.relevance.student.studentapplication.teacher;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TeacherService {
	
	@Autowired
	private TeacherRepostiary teacherrepostiary;

	//all teachers
	public List<Teacher> getAllTeachers( ) {
		List<Teacher> teacher= new ArrayList<>();
		teacherrepostiary.findAll().forEach(teacher::add);
		 return teacher;
	}
	
	//particular class teachers
	public List<Teacher> getAllTeacher(String cclassId) {
		List<Teacher> teacher= new ArrayList<>();
		teacherrepostiary.findByCclassId(cclassId).forEach(teacher::add);
		 return teacher;
	}

	public Optional<Teacher> getTeacher(String teacher_id) {
		
		return teacherrepostiary.findById(teacher_id);
	}

	public void addStudent(Teacher teacher) {
		teacherrepostiary.save(teacher);
	}

	public void updateTeacher(Teacher teacher, String teacher_id) {
		teacherrepostiary.save(teacher);
	}

	public void deleteTeacher(String teacher_id) {
		teacherrepostiary.deleteById(teacher_id);
	}

}
