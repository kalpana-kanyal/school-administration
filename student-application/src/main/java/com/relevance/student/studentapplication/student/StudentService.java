package com.relevance.student.studentapplication.student;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class StudentService {
	
	@Autowired
	private StudentRepostiary studentrepostiary;
	//all student in all classes
	public List<Student> getAllStudents() {
		List<Student> student= new ArrayList<>();
		studentrepostiary.findAll().forEach(student::add);
		 return student;
	}
	//PARTICULAR student in all classes
	public List<Student> getAllStudent(String cclassId) {
		List<Student> student= new ArrayList<>();
		studentrepostiary.findByCclassId(cclassId).forEach(student::add);
		 return student;
	}
	 
	public Optional<Student> getStudent(String student_id) {
		
		return studentrepostiary.findById(student_id);
	}

	public void addStudent(Student student) {
		studentrepostiary.save(student);
	}

	public void updateStudent(Student student, String student_id) {
		studentrepostiary.save(student);
	}

	public void deleteStudent(String student_id) {
		studentrepostiary.deleteById(student_id);
	}

}
