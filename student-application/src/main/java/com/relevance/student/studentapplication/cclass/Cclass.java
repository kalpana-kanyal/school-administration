package com.relevance.student.studentapplication.cclass;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.relevance.student.studentapplication.topic.Topic;

@Entity
public class Cclass {
    @Id 
    @Column(name="cclass_id")
	private String id;
    @Column(name="cclass_name")
	private String name;
	
	@ManyToOne
	@JoinColumn (name="topic_id")
	private Topic topic;
	public Cclass() {};
	
	
	public Topic getTopic() {
		return topic;
	}
	public void setTopic(Topic topic) {
		this.topic = topic;
	}
	public Cclass(String cclass_id, String cclass_name,String topicId) {
		super();
		this.id = cclass_id;
		this.name = cclass_name;
		
		this.topic=new Topic(topicId,"");
	}


	public String getCclass_id() {
		return id;
	}


	public void setCclass_id(String cclass_id) {
		this.id = cclass_id;
	}


	public String getCclass_name() {
		return name;
	}


	public void setCclass_name(String cclass_name) {
		this.name = cclass_name;
	}
	
}
