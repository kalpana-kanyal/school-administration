package com.relevance.student.studentapplication.teacher;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.relevance.student.studentapplication.cclass.Cclass;


@Entity
public class Teacher {
	@Id
	String teacher_id;
	public Cclass getCclass() {
		return cclass;
	}





	public void setCclass(Cclass cclass) {
		this.cclass = cclass;
	}





	String teacher_fname;
	String teacher_lname;
	String teacher_email;
	
	@ManyToOne
	@JoinColumn (name="cclass_id")
	private Cclass cclass;
	public Teacher(String teacher_id, String teacher_fname, String teacher_lname, String teacher_email,String cclass_id) {
		super();
		this.teacher_id = teacher_id;
		this.teacher_fname = teacher_fname;
		this.teacher_lname = teacher_lname;
		this.teacher_email = teacher_email;
		this.cclass=new Cclass(cclass_id,"","");
	}



	
	
	public Teacher()  {}





	public String getTeacher_id() {
		return teacher_id;
	}





	public void setTeacher_id(String teacher_id) {
		this.teacher_id = teacher_id;
	}





	public String getTeacher_fname() {
		return teacher_fname;
	}





	public void setTeacher_fname(String teacher_fname) {
		this.teacher_fname = teacher_fname;
	}





	public String getTeacher_lname() {
		return teacher_lname;
	}





	public void setTeacher_lname(String teacher_lname) {
		this.teacher_lname = teacher_lname;
	}





	public String getTeacher_email() {
		return teacher_email;
	}





	public void setTeacher_email(String teacher_email) {
		this.teacher_email = teacher_email;
	}


	

}
