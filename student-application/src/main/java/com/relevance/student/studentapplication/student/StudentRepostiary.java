package com.relevance.student.studentapplication.student;


import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;
//import org.springframework.web.bind.annotation.CrossOrigin;




@CrossOrigin(origins = "http://localhost:4200")
@Repository
public  interface StudentRepostiary extends CrudRepository<Student,String>{
	
//	public List<Student> findByCclassCclass_id(String CclassCclass_id);
	//public List<Cclass> findByTopicId(String topicId);
	
	//public List<Student> findByCclassCclass_id(String cclass_id);
	
	
	public List<Student> findByCclassId(String cclassId);
	//public List<Cclass> findByTopicId(String topicId);
	
}
