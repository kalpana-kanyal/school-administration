package com.relevance.student.studentapplication.teacher;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;
@CrossOrigin(origins = "http://localhost:4200")
@Repository
public  interface TeacherRepostiary extends CrudRepository<Teacher,String>{
	public List<Teacher> findByCclassId(String cclassId);

}
