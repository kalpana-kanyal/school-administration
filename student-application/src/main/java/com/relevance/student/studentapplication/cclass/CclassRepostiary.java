package com.relevance.student.studentapplication.cclass;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
@CrossOrigin(origins = "http://localhost:4200")
public interface CclassRepostiary extends CrudRepository<Cclass,String> {
   
	public List<Cclass> findByTopicId(String topicId);
	
	
	
}
