package com.relevance.student.studentapplication.cclass;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.relevance.student.studentapplication.topic.Topic;
@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class CclassController {
	
	@Autowired
	private CclassService courseService;
	
	@RequestMapping("/topics/{id}/cclass")
	public List<Cclass> getAllCclass(@PathVariable String id) {
		return courseService.getAllCclass(id) ;
	}
	@RequestMapping("/topics/{id}/cclass/{cclass_id}")
	public Optional<Cclass> get(@PathVariable String cclass_id) {
		return courseService.getCourse(cclass_id);
		
	}
	@RequestMapping(method=RequestMethod.POST,value="/topics/{topicId}/cclass")
	public void addCourse(@RequestBody Cclass cclass,@PathVariable String topicId) {
		
		cclass.setTopic(new Topic(topicId,""));
		courseService.addCourse(cclass);
	}
	@RequestMapping(method=RequestMethod.PUT,value="/topics/{id}/cclass/{cclass_id}")
	public void updateCourse(@RequestBody Cclass cclass,@PathVariable String topicId,@PathVariable String cclass_id) {
		cclass.setTopic(new Topic(topicId,""));
		courseService.updateCourse(cclass);
	}
	@RequestMapping(method=RequestMethod.DELETE,value="/cclass/{cclass_id}")
	public void deleteCourse(@PathVariable String cclass_id) {
		courseService.deleteCourse(cclass_id);
	}
	
	
//	@RequestMapping("/topics/{id}/cclass")
//	public List<Cclass> getAllCourse(@PathVariable String id) {
//		return courseService.getAllCourse(id) ;
//	}
//	@RequestMapping("/topics/{topicId}/cclass/{cclass_id}")
//	public Optional<Cclass> get(@PathVariable String cclass_id) {
//		return courseService.getCourse(cclass_id);
//		
//	}
//	@RequestMapping(method=RequestMethod.POST,value="/topics/{topicId}/cclass")
//	public void addCourse(@RequestBody Cclass cclass,@PathVariable String topicId) {
//		
//		cclass.setTopic(new Topic(topicId,""));
//		courseService.addCourse(cclass);
//	}
//	@RequestMapping(method=RequestMethod.PUT,value="/topics/{topicId}/cclass/{cclass_id}")
//	public void updateCourse(@RequestBody Cclass cclass,@PathVariable String topicId,@PathVariable String cclass_id) {
//		cclass.setTopic(new Topic(topicId,""));
//		courseService.updateCourse(cclass);
//	}
//	@RequestMapping(method=RequestMethod.DELETE,value="/topics/{id}/cclass/{cclass_id}")
//	public void deleteCourse(@PathVariable String cclass_id) {
//		courseService.deleteCourse(cclass_id);
//	}
}
