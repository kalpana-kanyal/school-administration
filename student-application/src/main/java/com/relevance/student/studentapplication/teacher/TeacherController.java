package com.relevance.student.studentapplication.teacher;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.relevance.student.studentapplication.cclass.Cclass;
@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class TeacherController {
	
	@Autowired
	 private TeacherService teacherservice;
	
	//all teachers in all classes
	@RequestMapping("/teachers")
	 public List<Teacher> getAllTeachers() {
		return teacherservice.getAllTeachers();
	}
	
	//particular class teachers
	@RequestMapping("/topics/{topicId}/cclass/{cclass_id}/teacher")
	 public List<Teacher> getAllTeacher(@PathVariable String topicId ,@PathVariable String cclass_id) {
		return teacherservice.getAllTeacher(cclass_id);
	}
	
	@RequestMapping("/topics/{topicId}/cclass/{cclass_id}/teacher/{teacher_id}")
	public  Optional<Teacher> getTeacher(@PathVariable String teacher_id ) {
		return  teacherservice.getTeacher(teacher_id);
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/topics/{topicId}/cclass/{cclass_id}/teacher")
	public void addTeacher(@RequestBody Teacher teacher,@PathVariable String topicId ,@PathVariable String cclass_id) {
		teacher.setCclass(new Cclass(cclass_id,"",""));
		teacherservice.addStudent(teacher); 
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/topics/{topicId}/cclass/{cclass_id}/teacher/{teacher_id}")
	public void updateTeacher(@RequestBody Teacher teacher,@PathVariable String teacher_id,@PathVariable String topicId ,@PathVariable String cclass_id) {
		teacher.setCclass(new Cclass(cclass_id,"",""));
		teacherservice.updateTeacher(teacher,teacher_id);
		
	}
	
	@RequestMapping(method=RequestMethod.DELETE,value="/topics/{topicId}/cclass/{cclass_id}/teacher/{teacher_id}")
	 public void deleteTeacher(@PathVariable String teacher_id) {
		teacherservice.deleteTeacher(teacher_id);
	}
	

}
