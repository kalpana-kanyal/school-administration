package com.relevance.student.studentapplication.topic;

import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
@CrossOrigin(origins = "http://localhost:4200")
public interface TopicRepostiary extends CrudRepository<Topic,String> {

}
